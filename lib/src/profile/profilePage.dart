import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  int _counter = 0;

  void _resetCounter() {
    setState(() {
      _counter = 0;
    });
  }

  void _increaseCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
            ButtonBar(
              children: [
                RaisedButton(
                  onPressed: _resetCounter,
                  child: Text("Reset"),
                ),
                RaisedButton(
                  onPressed: _increaseCounter,
                  child: Text("Increase"),
                ),
              ],
              alignment: MainAxisAlignment.center,
            )
          ],
        ),
      );
  }
}
