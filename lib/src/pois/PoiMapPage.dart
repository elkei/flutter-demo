import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

class PoiMapPage extends StatelessWidget {
  PoiMapPage({Key key}) : super(key: key);

  Widget build(BuildContext context) {
    return new FlutterMap(
      options: new MapOptions(
        center: new LatLng(48.35, 10.90),
        zoom: 13.0,
      ),
      layers: [
        new TileLayerOptions(
            urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            subdomains: ['a', 'b', 'c']
        ),
        new MarkerLayerOptions(
          markers: [
            new Marker(
              width: 80.0,
              height: 80.0,
              point: new LatLng(48.333889, 10.898333),
              builder: (ctx) =>
              new Container(
                child: Icon(
                  Icons.school,
                  size: 40,
                  color: Theme.of(context).accentColor,
                  semanticLabel: "Uni Augsburg",
                ),
              ),
            ),
            new Marker(
              width: 80.0,
              height: 80.0,
              point: new LatLng(48.3605745, 10.9032627),
              builder: (ctx) =>
              new Container(
                child: Icon(
                  Icons.theater_comedy,
                  size: 40,
                  color: Theme.of(context).accentColor,
                  semanticLabel: "Augsburger Puppenkiste",
                ),
              ),
            ),
            new Marker(
              width: 80.0,
              height: 80.0,
              point: new LatLng(48.3654907, 10.8856913),
              builder: (ctx) =>
              new Container(
                child: Icon(
                  Icons.train,
                  size: 40,
                  color: Theme.of(context).accentColor,
                  semanticLabel: "Hauptbahnhof",
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
